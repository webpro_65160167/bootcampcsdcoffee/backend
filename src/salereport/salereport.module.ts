import { Module } from '@nestjs/common';
import { SalereportController } from './salereport.controller';
import { SalereportService } from './salereport.service';

@Module({
  controllers: [SalereportController],
  providers: [SalereportService],
})
export class SalereportModule {}
