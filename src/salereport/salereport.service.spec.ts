import { Test, TestingModule } from '@nestjs/testing';
import { SalereportService } from './salereport.service';

describe('SalereportService', () => {
  let service: SalereportService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SalereportService],
    }).compile();

    service = module.get<SalereportService>(SalereportService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
