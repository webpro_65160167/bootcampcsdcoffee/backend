import { Body, Controller, Get, Post } from '@nestjs/common';
import { SalereportService } from './salereport.service';
import { CreateSaleReportDto } from './dto/create-SaleReportdto';

@Controller('salereport')
export class SalereportController {
  constructor(private saleReportSercvice: SalereportService) {}
  @Post('/reportSaleperDayB1')
  report1(@Body() createSaleReportDto: CreateSaleReportDto) {
    return this.saleReportSercvice.reportSalePerDayB1(createSaleReportDto);
  }

  @Post('/reportSaleperMonthB1')
  reportSalePerMonth(@Body() createSaleReportDto: CreateSaleReportDto) {
    return this.saleReportSercvice.reportSalePerMonthB1(createSaleReportDto);
  }

  @Get('/reportSaleperYearB1')
  reportSalePerYear() {
    return this.saleReportSercvice.reportSalePerYearB1();
  }

  @Post('/reportSaleperDayB2')
  report2(@Body() createSaleReportDto: CreateSaleReportDto) {
    return this.saleReportSercvice.reportSalePerDayB2(createSaleReportDto);
  }

  @Post('/reportSaleperMonthB2')
  reportSalePerMonthB2(@Body() createSaleReportDto: CreateSaleReportDto) {
    return this.saleReportSercvice.reportSalePerMonthB2(createSaleReportDto);
  }

  @Get('/reportSaleperYearB2')
  reportSalePerYearB2() {
    return this.saleReportSercvice.reportSalePerYearB2();
  }

  @Post('/reportSaleperDayB3')
  report3(@Body() createSaleReportDto: CreateSaleReportDto) {
    return this.saleReportSercvice.reportSalePerDayB3(createSaleReportDto);
  }

  @Post('/reportSaleperMonthB3')
  reportSalePerMonthB3(@Body() createSaleReportDto: CreateSaleReportDto) {
    return this.saleReportSercvice.reportSalePerMonthB3(createSaleReportDto);
  }

  @Get('/reportSaleperYearB3')
  reportSalePerYearB3() {
    return this.saleReportSercvice.reportSalePerYearB3();
  }

  @Get('/reportSaleTotalB1')
  reportSaleTotal() {
    return this.saleReportSercvice.reportSaleTotal();
  }

  @Get('/reportSaleTotalB2')
  reportSaleTotalB2() {
    return this.saleReportSercvice.reportSaleTotalB2();
  }

  @Get('/reportSaleTotalB3')
  reportSaleTotalB3() {
    return this.saleReportSercvice.reportSaleTotalB3();
  }

  @Get('/reportSaleTotalMonthB1')
  reportSaleTotalMonthB1() {
    return this.saleReportSercvice.reportSaleTotalMonthB1();
  }

  @Get('/reportSaleTotalMonthB2')
  reportSaleTotalMonthB2() {
    return this.saleReportSercvice.reportSaleTotalMonthB2();
  }

  @Get('/reportSaleTotalMonthB3')
  reportSaleTotalMonthB3() {
    return this.saleReportSercvice.reportSaleTotalMonthB3();
  }
}
