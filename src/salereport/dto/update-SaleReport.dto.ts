import { PartialType } from '@nestjs/swagger';
import { CreateSaleReportDto } from './create-SaleReportdto';

export class UpdateSaleReportDto extends PartialType(CreateSaleReportDto) {}
