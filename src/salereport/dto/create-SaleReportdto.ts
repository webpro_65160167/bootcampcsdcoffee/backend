export class CreateSaleReportDto {
  day: number;
  month: string;
  year: string;
}
