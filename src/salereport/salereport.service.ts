import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';
import { CreateSaleReportDto } from './dto/create-SaleReportdto';

@Injectable()
export class SalereportService {
  constructor(private dataSource: DataSource) {}

  async reportSalePerDayB1(createSaleReportDto: CreateSaleReportDto) {
    return this.dataSource.query(`
    SELECT DATE_FORMAT(o.created, '%Y-%m-%d') AS order_date,
    SUM(o.total) AS total_sum
FROM \`order\` AS o
JOIN \`user\` AS u ON o.userId = u.id
WHERE YEAR(o.created) = '${createSaleReportDto.year}'
AND MONTH(o.created) = '${createSaleReportDto.month}'
AND u.branchId = 1
GROUP BY order_date;
    `);
  }
  reportSalePerMonthB1(createSaleReportDto: CreateSaleReportDto) {
    return this.dataSource
      .query(`SELECT DATE_FORMAT(o.created, '%Y-%m') AS order_month,
      SUM(total) AS total_sum
FROM \`order\` AS o
JOIN \`user\` AS u ON o.userId = u.id
WHERE YEAR(o.created) = '${createSaleReportDto.year}'
AND u.branchId = 1
GROUP BY order_month;`);
  }
  reportSalePerYearB1() {
    return this.dataSource
      .query(`SELECT DATE_FORMAT(o.created, '%Y') AS order_year,
    SUM(total) AS total_sum
FROM \`order\` AS o
JOIN \`user\` AS u ON o.userId = u.id
AND u.branchId = 1
GROUP BY order_year;`);
  }

  async reportSalePerDayB2(createSaleReportDto: CreateSaleReportDto) {
    return this.dataSource.query(`
    SELECT DATE_FORMAT(o.created, '%Y-%m-%d') AS order_date,
    SUM(o.total) AS total_sum
FROM \`order\` AS o
JOIN \`user\` AS u ON o.userId = u.id
WHERE YEAR(o.created) = '${createSaleReportDto.year}'
AND MONTH(o.created) = '${createSaleReportDto.month}'
AND u.branchId = 2
GROUP BY order_date;
    `);
  }
  reportSalePerMonthB2(createSaleReportDto: CreateSaleReportDto) {
    return this.dataSource
      .query(`SELECT DATE_FORMAT(o.created, '%Y-%m') AS order_month,
      SUM(total) AS total_sum
FROM \`order\` AS o
JOIN \`user\` AS u ON o.userId = u.id
WHERE YEAR(o.created) = '${createSaleReportDto.year}'
AND u.branchId = 2
GROUP BY order_month;`);
  }
  reportSalePerYearB2() {
    return this.dataSource
      .query(`SELECT DATE_FORMAT(o.created, '%Y') AS order_year,
    SUM(total) AS total_sum
FROM \`order\` AS o
JOIN \`user\` AS u ON o.userId = u.id
AND u.branchId = 2
GROUP BY order_year;`);
  }

  async reportSalePerDayB3(createSaleReportDto: CreateSaleReportDto) {
    return this.dataSource.query(`
    SELECT DATE_FORMAT(o.created, '%Y-%m-%d') AS order_date,
    SUM(o.total) AS total_sum
FROM \`order\` AS o
JOIN \`user\` AS u ON o.userId = u.id
WHERE YEAR(o.created) = '${createSaleReportDto.year}'
AND MONTH(o.created) = '${createSaleReportDto.month}'
AND u.branchId = 3
GROUP BY order_date;
    `);
  }
  reportSalePerMonthB3(createSaleReportDto: CreateSaleReportDto) {
    return this.dataSource
      .query(`SELECT DATE_FORMAT(o.created, '%Y-%m') AS order_month,
      SUM(total) AS total_sum
FROM \`order\` AS o
JOIN \`user\` AS u ON o.userId = u.id
WHERE YEAR(o.created) = '${createSaleReportDto.year}'
AND u.branchId = 3
GROUP BY order_month;`);
  }
  reportSalePerYearB3() {
    return this.dataSource
      .query(`SELECT DATE_FORMAT(o.created, '%Y') AS order_year,
    SUM(total) AS total_sum
FROM \`order\` AS o
JOIN \`user\` AS u ON o.userId = u.id
AND u.branchId = 3
GROUP BY order_year;`);
  }

  reportSaleTotal() {
    return this.dataSource
      .query(`SELECT DATE_FORMAT(o.created, '%Y-%m-%d') AS total_sale,
      SUM(o.total) AS total_sum
  FROM \`order\` AS o
  JOIN \`user\` AS u ON o.userId = u.id
  WHERE u.branchId = 1
  GROUP BY total_sale
  ORDER BY total_sum ASC;
  `);
  }
  reportSaleTotalB2() {
    return this.dataSource
      .query(`SELECT DATE_FORMAT(o.created, '%Y-%m-%d') AS total_sale,
      SUM(o.total) AS total_sum
  FROM \`order\` AS o
  JOIN \`user\` AS u ON o.userId = u.id
  WHERE u.branchId = 2
  GROUP BY total_sale
  ORDER BY total_sum ASC;
  `);
  }
  reportSaleTotalB3() {
    return this.dataSource
      .query(`SELECT DATE_FORMAT(o.created, '%Y-%m-%d') AS total_sale,
      SUM(o.total) AS total_sum
  FROM \`order\` AS o
  JOIN \`user\` AS u ON o.userId = u.id
  WHERE u.branchId = 3
  GROUP BY total_sale
  ORDER BY total_sum ASC;
  `);
  }

  reportSaleTotalMonthB1() {
    return this.dataSource.query(`SELECT u.branchId,SUM(o.total) AS total_sum
      FROM \`order\` AS o
      JOIN \`user\` AS u ON o.userId = u.id
      WHERE u.branchId = 1
  `);
  }

  reportSaleTotalMonthB2() {
    return this.dataSource.query(`SELECT u.branchId,SUM(o.total) AS total_sum
      FROM \`order\` AS o
      JOIN \`user\` AS u ON o.userId = u.id
      WHERE u.branchId = 2
  `);
  }

  reportSaleTotalMonthB3() {
    return this.dataSource.query(`SELECT u.branchId,SUM(o.total) AS total_sum
      FROM \`order\` AS o
      JOIN \`user\` AS u ON o.userId = u.id
      WHERE u.branchId = 3
  `);
  }
}
