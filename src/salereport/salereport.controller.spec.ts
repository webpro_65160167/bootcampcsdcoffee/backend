import { Test, TestingModule } from '@nestjs/testing';
import { SalereportController } from './salereport.controller';

describe('SalereportController', () => {
  let controller: SalereportController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SalereportController],
    }).compile();

    controller = module.get<SalereportController>(SalereportController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
