import { Module } from '@nestjs/common';
import { StockService } from './stock.service';
import { StockController } from './stock.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stock } from './entities/Stock.entity';
import { Branch } from 'src/branchs/entities/branch.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Stock, Branch, User])],
  controllers: [StockController],
  providers: [StockService],
})
export class StockModule {}
