import { Injectable } from '@nestjs/common';
import { CreateStockDto } from './dto/create-Stock.dto';
import { UpdateStockDto } from './dto/update-Stock.dto';
import { Stock } from './entities/Stock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class StockService {
  constructor(
    @InjectRepository(Stock) private stockRepository: Repository<Stock>,
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}
  async create(createStockDto: CreateStockDto) {
    const stock = new Stock();
    const user = await this.userRepository.findOne({
      where: { id: createStockDto.userId },
      relations: { roles: true, branch: true },
    });
    stock.name = createStockDto.name;
    stock.priceUnit = createStockDto.priceUnit;
    stock.quantity = createStockDto.quantity;
    stock.value = createStockDto.quantity * createStockDto.priceUnit;
    stock.branch = user.branch;
    return this.stockRepository.save(stock);
  }

  async insertStock(createStockDto: CreateStockDto) {
    try {
      await this.stockRepository.query(
        `CALL insertStock('${createStockDto.name}', '${createStockDto.priceUnit}', '${createStockDto.quantity}', @pResult, @pMessage)`,
      );
      const result = await this.stockRepository.query(
        `SELECT @pResult as XResult, @pMessage as xMessage`,
      );
      console.log(result[0]);
      return result[0];
    } catch (error) {
      console.log(error);
    }
  }
  findAll() {
    return this.stockRepository.find({ relations: ['branch'] });
  }
  findOne(id: number) {
    return this.stockRepository.findOneBy({ id: id });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    const stock = await this.stockRepository.findOneOrFail({
      where: { id },
    });
    const quantity = stock.quantity;
    const priceUnit = stock.priceUnit;
    stock.name = updateStockDto.name;
    stock.quantity = updateStockDto.quantity;
    stock.priceUnit = updateStockDto.priceUnit;
    if (stock.quantity == undefined) {
      stock.value = updateStockDto.priceUnit * quantity;
    } else if (stock.priceUnit == undefined) {
      stock.value = priceUnit * updateStockDto.quantity;
    } else {
      stock.value = updateStockDto.priceUnit * updateStockDto.quantity;
    }

    await this.stockRepository.save(stock);
    const result = await this.stockRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteStock = await this.stockRepository.findOneOrFail({
      where: { id },
    });
    await this.stockRepository.remove(deleteStock);
    return deleteStock;
  }
}
