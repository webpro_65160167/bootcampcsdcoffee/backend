export class CreateStockDto {
  name: string;
  priceUnit: number;
  quantity: number;
  userId: number;
}
