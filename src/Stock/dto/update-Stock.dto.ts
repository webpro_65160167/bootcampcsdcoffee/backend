import { PartialType } from '@nestjs/swagger';
import { CreateStockDto } from './create-Stock.dto';

export class UpdateStockDto extends PartialType(CreateStockDto) {}
