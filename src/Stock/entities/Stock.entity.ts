import { Branch } from 'src/branchs/entities/branch.entity';
import { CheckStockItem } from 'src/check-stock/entities/check-stock-item.entity';
import { StockReceiptItems } from 'src/stock-receipt/entities/StockReceiptItem.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  priceUnit: number;

  @Column()
  quantity: number;

  @Column()
  value: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => StockReceiptItems,
    (stockReceiptItems) => stockReceiptItems.stock,
  )
  stockReceiptItems: StockReceiptItems[];

  @OneToMany(() => CheckStockItem, (checkStockItems) => checkStockItems.stock)
  checkStockItems: CheckStockItem[];

  @ManyToOne(() => Branch, (branch) => branch.stock)
  branch: Branch[];
}
