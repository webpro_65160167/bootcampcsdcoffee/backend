import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StockModule } from './Stock/Stock.module';
import { RolesModule } from './roles/roles.module';
import { TypesModule } from './types/types.module';
import { ProductsModule } from './products/products.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrdersModule } from './orders/orders.module';
import { DataSource } from 'typeorm';
import { Role } from './roles/entities/role.entity';
import { Order } from './orders/entities/order.entity';
import { Type } from './types/entities/type.entity';
import { Product } from './products/entities/product.entity';
import { OrderItem } from './orders/entities/orderItem.entity';
import { AuthModule } from './auth/auth.module';
import { Stock } from './Stock/entities/Stock.entity';
import { StockReceipt } from './stock-receipt/entities/StockReceipt.entity';
import { StockReceiptItems } from './stock-receipt/entities/StockReceiptItem.entity';
import { StockReceiptModule } from './stock-receipt/stock-receipt.module';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { MemberModule } from './member/member.module';
import { Member } from './member/entities/member.entity';
import { BranchsModule } from './branchs/branchs.module';
import { Branch } from './branchs/entities/branch.entity';
import { SalarysModule } from './salarys/salarys.module';
import { CheckStockModule } from './check-stock/check-stock.module';
import { CheckStock } from './check-stock/entities/check-stock.entity';
import { CheckStockItem } from './check-stock/entities/check-stock-item.entity';
import { Salary } from './salarys/entities/salary.entity';
import { User } from './users/entities/user.entity';
import { UsersModule } from './users/users.module';
import { CheckTime } from './checktime/entities/checktime.entity';
import { Promotion } from './promotion/entities/promotion.entity';
import { CheckTimeModule } from './checktime/checktime.module';
import { Expense } from './expenses/entities/expense.entity';
import { ExpensesModule } from './expenses/expenses.module';
import { ReportStockReceiptModule } from './report/report-stock-receipt/report-stock-receipt.module';
import { PromotionModule } from './promotion/promotion.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { SalereportModule } from './salereport/salereport.module';
import { SalaryReportModule } from './salary-report/salary-report.module';
import { ReportCheckStockModule } from './report/report-check-stock/report-check-stock.module';

@Module({
  imports: [
    // TypeOrmModule.forRoot({
    //   type: 'sqlite',
    //   database: 'mydb.sqlite',
    //   entities: [
    //     User,
    //     Stock,
    //     Role,
    //     Order,
    //     OrderItem,
    //     Type,
    //     Product,
    //     Promotion,
    //     StockReceipt,
    //     StockReceiptItems,
    //     Employee,
    //     Member,
    //     Branch,
    //     CheckStock,
    //     CheckStockItem,
    //     Salary,
    //     CheckTime,
    //     Expense,
    //   ],
    //   synchronize: true,
    //   logging: false,
    // }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'angsila.informatics.buu.ac.th',
      port: 3306,
      username: 'cscamp04',
      password: 'L4wGgg0NLg',
      database: 'cscamp04',
      entities: [
        User,
        Stock,
        Role,
        Order,
        OrderItem,
        Type,
        Product,
        Promotion,
        StockReceipt,
        StockReceiptItems,
        Employee,
        Member,
        Branch,
        CheckStock,
        CheckStockItem,
        Salary,
        CheckTime,
        Expense,
      ],
      synchronize: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    UsersModule,
    RolesModule,
    OrdersModule,
    TypesModule,
    ProductsModule,
    AuthModule,
    StockReceiptModule,
    StockModule,
    EmployeesModule,
    PromotionModule,
    MemberModule,
    BranchsModule,
    SalarysModule,
    CheckStockModule,
    CheckTimeModule,
    ExpensesModule,
    ReportStockReceiptModule,
    SalereportModule,
    SalaryReportModule,
    ReportCheckStockModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
