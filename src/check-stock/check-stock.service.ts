import { Injectable } from '@nestjs/common';
import { CreateCheckStockDto } from './dto/create-check-stock.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Stock } from 'src/Stock/entities/Stock.entity';
import { Repository } from 'typeorm';
import { CheckStock } from './entities/check-stock.entity';
import { CheckStockItem } from './entities/check-stock-item.entity';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class CheckStockService {
  @InjectRepository(Stock) private stockRepository: Repository<Stock>;
  @InjectRepository(CheckStock)
  private checkStocksRepository: Repository<CheckStock>;
  @InjectRepository(CheckStockItem)
  private checkStockItemsRepository: Repository<CheckStockItem>;
  @InjectRepository(User) private usersRepository: Repository<User>;
  async create(createCheckStockDto: CreateCheckStockDto) {
    const checkStock = new CheckStock();
    const user = await this.usersRepository.findOneBy({
      id: createCheckStockDto.userId,
    });
    checkStock.employee = user;
    checkStock.employeeId = user.id;
    checkStock.employeeName = user.fullName;
    checkStock.checkStockDate = new Date();
    checkStock.checkStockItems = [];
    checkStock.checkStockAmount = 0;
    for (const sri of createCheckStockDto.checkStockItems) {
      const checkStockItem = new CheckStockItem();
      checkStockItem.stock = await this.stockRepository.findOneBy({
        id: sri.stockId,
      });
      checkStockItem.name = checkStockItem.stock.name;
      checkStockItem.lastCheckQty = checkStockItem.stock.quantity;
      checkStockItem.remain = sri.remain;
      checkStockItem.used = checkStockItem.lastCheckQty - checkStockItem.remain;
      checkStock.checkStockAmount += 1;
      await this.checkStockItemsRepository.save(checkStockItem);
      checkStock.checkStockItems.push(checkStockItem);
      const stock = await this.stockRepository.findOneBy({
        id: sri.stockId,
      });
      stock.name = stock.name;
      stock.quantity = sri.remain;
      stock.value = stock.priceUnit * stock.quantity;
      await this.stockRepository.save(stock);
    }
    return this.checkStocksRepository.save(checkStock);
  }

  findAll() {
    return this.checkStocksRepository.find({
      relations: ['checkStockItems', 'employee', 'employee.branch'],
      order: {
        id: 'ASC',
      },
    });
  }

  findOne(id: number) {
    return this.checkStocksRepository.find({
      where: { id },
      relations: {
        checkStockItems: true,
        // employee: true,
      },
    });
  }
  async ShowStatusStock(month: string, year: string) {
    try {
      const result = await this.checkStocksRepository.query(
        `CALL ShowStatusStock('${month}','${year}');`,
      );
      return result[0];
    } catch (error) {
      console.log(error);
    }
  }
}
