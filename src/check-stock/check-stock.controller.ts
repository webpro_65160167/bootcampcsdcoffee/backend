import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { CheckStockService } from './check-stock.service';
import { CreateCheckStockDto } from './dto/create-check-stock.dto';

@Controller('check-stock')
export class CheckStockController {
  constructor(private readonly checkStockService: CheckStockService) {}

  @Post()
  create(@Body() createCheckStockDto: CreateCheckStockDto) {
    return this.checkStockService.create(createCheckStockDto);
  }

  @Get()
  findAll() {
    return this.checkStockService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkStockService.findOne(+id);
  }

  @Get('showstatus/:month/:year')
  showStatusStock(@Param('month') month: string, @Param('year') year: string) {
    return this.checkStockService.ShowStatusStock(month, year);
  }
}
