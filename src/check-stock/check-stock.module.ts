import { Module } from '@nestjs/common';
import { CheckStockService } from './check-stock.service';
import { CheckStockController } from './check-stock.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stock } from 'src/Stock/entities/Stock.entity';
import { CheckStock } from './entities/check-stock.entity';
import { CheckStockItem } from './entities/check-stock-item.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Stock, CheckStock, CheckStockItem, User]),
  ],
  controllers: [CheckStockController],
  providers: [CheckStockService],
})
export class CheckStockModule {}
