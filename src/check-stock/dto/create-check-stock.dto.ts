export class CreateCheckStockDto {
  checkStockItems: {
    stockId: number;
    remain: number;
  }[];
  userId: number;
}
