import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { CheckStock } from './check-stock.entity';
import { Stock } from 'src/Stock/entities/Stock.entity';

@Entity()
export class CheckStockItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  stockId: number;

  @Column()
  lastCheckQty: number;

  @Column()
  remain: number;

  @Column()
  used: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => CheckStock, (checkStock) => checkStock.checkStockItems, {
    onDelete: 'CASCADE',
  })
  checkStock: CheckStock;

  @ManyToOne(() => Stock, (stock) => stock.checkStockItems)
  stock: Stock;
}
