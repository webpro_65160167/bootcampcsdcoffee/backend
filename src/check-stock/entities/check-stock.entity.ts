import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { CheckStockItem } from './check-stock-item.entity';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class CheckStock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  checkStockDate: Date;

  @Column()
  checkStockAmount: number;

  @Column()
  employeeId: number;

  @Column()
  employeeName: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => CheckStockItem,
    (checkStockItem) => checkStockItem.checkStock,
  )
  checkStockItems: CheckStockItem[];

  @ManyToOne(() => User, (employee) => employee.checkStocks)
  employee: User;
}
