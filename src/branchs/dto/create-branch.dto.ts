export class CreateBranchDto {
  name: string;
  address: string;
  email: string;
  tel: string;
  openDate: Date;
}
