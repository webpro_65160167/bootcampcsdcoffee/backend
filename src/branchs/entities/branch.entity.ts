import { Stock } from 'src/Stock/entities/Stock.entity';
import { Expense } from 'src/expenses/entities/expense.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  address: string;

  @Column()
  email: string;

  @Column()
  tel: string;

  @Column()
  openDate: Date;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => Expense, (expense) => expense.branchName)
  expenses: Expense;

  @OneToMany(() => User, (user) => user.branch)
  user: User;

  @OneToMany(() => Stock, (stock) => stock.branch)
  stock: User;
}
