import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Branch } from './entities/branch.entity';

@Injectable()
export class BranchsService {
  constructor(
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
  ) {}

  create(createBranchDto: CreateBranchDto) {
    return this.branchRepository.save(createBranchDto);
  }

  findAll() {
    return this.branchRepository.find();
  }

  findOne(id: number) {
    return this.branchRepository.findOne({ where: { id } });
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    const updateBranch = await this.branchRepository.findOneOrFail({
      where: { id },
    });
    updateBranch.name = updateBranchDto.name;
    updateBranch.address = updateBranchDto.address;
    updateBranch.email = updateBranchDto.email;
    updateBranch.tel = updateBranchDto.tel;
    updateBranch.openDate = updateBranchDto.openDate;
    await this.branchRepository.save(updateBranch);

    const result = await this.branchRepository.findOne({
      where: { id },
    });

    return result;
  }

  async remove(id: number) {
    const deleteBranch = await this.branchRepository.findOneOrFail({
      where: { id },
    });
    await this.branchRepository.remove(deleteBranch);
    return deleteBranch;
  }
}
