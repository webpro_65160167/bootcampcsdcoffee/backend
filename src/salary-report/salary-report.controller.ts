import { Controller, Post, Body, Param, Get } from '@nestjs/common';
import { SalaryReportService } from './salary-report.service';
import { CreateSalaryReportDto } from './dto/create-salary-report.dto';

@Controller('salary-report')
export class SalaryReportController {
  constructor(private readonly salaryReportService: SalaryReportService) {}

  @Post('/reportSalaryPerMonth')
  reportSalaryPerMonth(@Body() createSalaryReportDto: CreateSalaryReportDto) {
    return this.salaryReportService.reportSalaryPerMonth(createSalaryReportDto);
  }

  @Post('/reportSalaryPerYear')
  reportSalaryPerYear() {
    return this.salaryReportService.reportSalaryPerYear();
  }

  @Post('/getRoleName')
  getRoleName(@Body() createSalaryReportDto: CreateSalaryReportDto) {
    return this.salaryReportService.getRoleName(createSalaryReportDto);
  }

  @Get('salarybyemp/:id')
  getSalaryByEmp(@Param('id') id: number) {
    return this.salaryReportService.getSalaryByEmp(id);
  }
}
