import { Injectable } from '@nestjs/common';
import { CreateSalaryReportDto } from './dto/create-salary-report.dto';
import { DataSource, Repository } from 'typeorm';
import { Salary } from 'src/salarys/entities/salary.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class SalaryReportService {
  @InjectRepository(Salary)
  private stockReceiptRepository: Repository<Salary>;
  constructor(private dataSource: DataSource) {}

  reportSalaryPerMonth(createSalaryReportDto: CreateSalaryReportDto) {
    return this.dataSource.query(`
    SELECT DATE_FORMAT(payDate, '%Y-%m') AS pay_month,
          SUM(netSalary) AS netSalary_sum
    FROM \`salary\`
    WHERE YEAR(payDate) = '${createSalaryReportDto.year}'
    GROUP BY pay_month;
    `);
  }

  reportSalaryPerYear() {
    return this.dataSource.query(`
    SELECT DATE_FORMAT(payDate, '%Y') AS pay_year,
          SUM(netSalary) AS netSalary_sum
    FROM \`salary\`
    GROUP BY pay_year;
    `);
  }

  getRoleName(createSalaryReportDto: CreateSalaryReportDto) {
    return this.dataSource.query(`
    SELECT user.id AS user_id, role.name AS role_name
    FROM user
    LEFT JOIN user_roles_role ON user.id = user_roles_role.userId
    LEFT JOIN role ON user_roles_role.roleId = role.id
    WHERE user.id = '${createSalaryReportDto.user_Id}';
`);
  }

  async getSalaryByEmp(id: number) {
    try {
      const result = await this.stockReceiptRepository.query(
        `CALL GetSalaryByEmpId('${id}');`,
      );
      return result[0];
    } catch (error) {
      console.log(error);
    }
  }
}
