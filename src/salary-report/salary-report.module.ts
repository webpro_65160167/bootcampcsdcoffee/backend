import { Module } from '@nestjs/common';
import { SalaryReportService } from './salary-report.service';
import { SalaryReportController } from './salary-report.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Salary } from 'src/salarys/entities/salary.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Salary])],
  controllers: [SalaryReportController],
  providers: [SalaryReportService],
})
export class SalaryReportModule {}
