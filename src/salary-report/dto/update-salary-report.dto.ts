import { PartialType } from '@nestjs/swagger';
import { CreateSalaryReportDto } from './create-salary-report.dto';

export class UpdateSalaryReportDto extends PartialType(CreateSalaryReportDto) {}
