export class CreateSalaryReportDto {
  day: number;
  month: string;
  year: string;
  user_Id: number;
}
