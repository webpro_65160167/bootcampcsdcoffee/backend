import { Module } from '@nestjs/common';
import { ReportStockReceiptController } from './report-stock-receipt.controller';
import { ReportStockReceiptService } from './report-stock-receipt.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StockReceipt } from 'src/stock-receipt/entities/StockReceipt.entity';

@Module({
  imports: [TypeOrmModule.forFeature([StockReceipt])],
  controllers: [ReportStockReceiptController],
  providers: [ReportStockReceiptService],
})
export class ReportStockReceiptModule {}
