import { Test, TestingModule } from '@nestjs/testing';
import { ReportStockReceiptController } from './report-stock-receipt.controller';

describe('ReportStockReceiptController', () => {
  let controller: ReportStockReceiptController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReportStockReceiptController],
    }).compile();

    controller = module.get<ReportStockReceiptController>(
      ReportStockReceiptController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
