import { Test, TestingModule } from '@nestjs/testing';
import { ReportStockReceiptService } from './report-stock-receipt.service';

describe('ReportStockReceiptService', () => {
  let service: ReportStockReceiptService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReportStockReceiptService],
    }).compile();

    service = module.get<ReportStockReceiptService>(ReportStockReceiptService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
