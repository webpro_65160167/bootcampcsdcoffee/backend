import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { StockReceipt } from 'src/stock-receipt/entities/StockReceipt.entity';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class ReportStockReceiptService {
  @InjectRepository(StockReceipt)
  private stockReceiptRepository: Repository<StockReceipt>;
  constructor(private dataSource: DataSource) {}

  async showStockExpenseReportPermonth(
    month: string,
    year: string,
    bId: number,
  ) {
    try {
      const result = await this.stockReceiptRepository.query(
        `CALL showStockExpenseReportPermonth('${month}','${year}', ${bId});`,
      );
      return result[0];
    } catch (error) {
      console.log(error);
    }
  }

  async showStockQtyReceiptReportPerMonth(
    month: string,
    year: string,
    bId: number,
  ) {
    try {
      const result = await this.stockReceiptRepository.query(
        `CALL showStockQtyReceiptReportPerMonth('${month}','${year}',${bId});`,
      );
      return result[0];
    } catch (error) {
      console.log(error);
    }
  }

  async showStockTotalPricePerYear(year: string, bId: number) {
    try {
      const result = await this.stockReceiptRepository.query(
        `CALL showStockTotalPricePerYear('${year}',${bId});`,
      );
      return result[0];
    } catch (error) {
      console.log(error);
    }
  }

  async showStockReceiptPerDay(
    year: string,
    month: string,
    day: string,
    bId: number,
  ) {
    try {
      const result = await this.stockReceiptRepository.query(
        `CALL showStockReceiptPerDay('${year}','${month}','${day}', ${bId});`,
      );
      return result[0];
    } catch (error) {
      console.log(error);
    }
  }
}
