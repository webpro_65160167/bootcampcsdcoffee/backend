import { Controller, Get, Param } from '@nestjs/common';
import { ReportStockReceiptService } from './report-stock-receipt.service';

@Controller('report-stock-receipt')
export class ReportStockReceiptController {
  constructor(
    private readonly reportstockReceiptService: ReportStockReceiptService,
  ) {}
  @Get('show-stock-expense-report-per-month/:month/:year/:bId')
  showStockExpenseReportPermonth(
    @Param('month') month: string,
    @Param('year') year: string,
    @Param('bId') bId: number,
  ) {
    return this.reportstockReceiptService.showStockExpenseReportPermonth(
      month,
      year,
      bId,
    );
  }

  @Get('show-stock-qty-receipt-report-per-month/:month/:year/:bId')
  showStockQtyReceiptReportPerMonth(
    @Param('month') month: string,
    @Param('year') year: string,
    @Param('bId') bId: number,
  ) {
    return this.reportstockReceiptService.showStockQtyReceiptReportPerMonth(
      month,
      year,
      bId,
    );
  }
  @Get('show-stock-total-price-per-year/:year/:bId')
  showStockTotalPricePerYear(
    @Param('year') year: string,
    @Param('bId') bId: number,
  ) {
    return this.reportstockReceiptService.showStockTotalPricePerYear(year, bId);
  }

  @Get('show-stock-receipt-per-day/:year/:month/:day/:bId')
  showStockReceiptPerDay(
    @Param('year') year: string,
    @Param('month') month: string,
    @Param('day') day: string,
    @Param('bId') bId: number,
  ) {
    return this.reportstockReceiptService.showStockReceiptPerDay(
      year,
      month,
      day,
      bId,
    );
  }
}
