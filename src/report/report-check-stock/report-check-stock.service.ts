import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CheckStock } from 'src/check-stock/entities/check-stock.entity';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class ReportCheckStockService {
  @InjectRepository(CheckStock)
  private checkStockRepository: Repository<CheckStock>;
  constructor(private dataSource: DataSource) {}

  async showUsedStockPerMonth(month: string, year: string, bId: number) {
    try {
      const result = await this.checkStockRepository.query(
        `CALL showUsedStockPerMonth('${month}','${year}', ${bId});`,
      );
      return result[0];
    } catch (error) {
      console.log(error);
    }
  }
  async showRemainStockPerMonth(month: string, year: string, bId: number) {
    try {
      const result = await this.checkStockRepository.query(
        `CALL showRemainStockPerMonth('${month}','${year}',${bId});`,
      );
      return result[0];
    } catch (error) {
      console.log(error);
    }
  }
}
