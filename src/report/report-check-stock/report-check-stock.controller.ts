import { Controller, Get, Param } from '@nestjs/common';
import { ReportCheckStockService } from './report-check-stock.service';

@Controller('report-check-stock')
export class ReportCheckStockController {
  constructor(
    private readonly reportCheckStockService: ReportCheckStockService,
  ) {}
  @Get('show-used-stock-per-month/:month/:year/:bid')
  showUsedStockPerMonth(
    @Param('month') month: string,
    @Param('year') year: string,
    @Param('bid') bId: number,
  ) {
    return this.reportCheckStockService.showUsedStockPerMonth(month, year, bId);
  }

  @Get('show-remain-stock-per-month/:month/:year/:bId')
  showRemainStockPerMonth(
    @Param('month') month: string,
    @Param('year') year: string,
    @Param('bId') bId: number,
  ) {
    return this.reportCheckStockService.showRemainStockPerMonth(
      month,
      year,
      bId,
    );
  }
}
