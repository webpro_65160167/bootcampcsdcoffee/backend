import { Module } from '@nestjs/common';
import { ReportCheckStockController } from './report-check-stock.controller';
import { ReportCheckStockService } from './report-check-stock.service';
import { CheckStock } from 'src/check-stock/entities/check-stock.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([CheckStock])],
  controllers: [ReportCheckStockController],
  providers: [ReportCheckStockService],
})
export class ReportCheckStockModule {}
