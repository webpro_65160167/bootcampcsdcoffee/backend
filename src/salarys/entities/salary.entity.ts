import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.salarys)
  employee: User;

  @Column()
  employee_Id: number;

  @Column()
  employee_Name: string;

  @Column()
  employee_BankAcc: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  payDate: Date;

  @Column()
  baseSalary: number;

  @Column()
  increaseSalary: number;

  @Column()
  salaryReduc: number;

  @Column()
  netSalary: number;

  @Column()
  status: string;
}
