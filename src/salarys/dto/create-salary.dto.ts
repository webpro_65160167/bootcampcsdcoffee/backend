import { User } from 'src/users/entities/user.entity';

export class CreateSalaryDto {
  employee: User[];
  employee_Id: number;
  increaseSalary: number;
  salaryReduc: number;
  status: string;
}
