import { Injectable } from '@nestjs/common';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Salary } from './entities/salary.entity';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { Role } from 'src/roles/entities/role.entity';
import { CheckTime } from 'src/checktime/entities/checktime.entity';

@Injectable()
export class SalarysService {
  constructor(
    @InjectRepository(Salary)
    private salarysRepository: Repository<Salary>,
    @InjectRepository(User) private empsRepository: Repository<User>,
    @InjectRepository(Role) private rolsesRepository: Repository<Role>,
    @InjectRepository(CheckTime)
    private checkTimesRepository: Repository<CheckTime>,
  ) {}

  async create(createSalaryDto: CreateSalaryDto) {
    const salary = new Salary();
    const employee = await this.empsRepository.findOne({
      where: { id: createSalaryDto.employee_Id },
      relations: { roles: true },
    });
    console.log(employee);

    salary.employee = employee;
    salary.employee_Id = employee.id;
    salary.employee_Name = employee.fullName;
    salary.employee_BankAcc = employee.bankAcc;
    salary.employee.roles = employee.roles;
    salary.salaryReduc = createSalaryDto.salaryReduc;
    console.log(createSalaryDto.increaseSalary);
    console.log(createSalaryDto.salaryReduc);
    const indexEmployee = employee.roles.findIndex(
      (item) => item.name == 'Employee',
    );
    const indexManager = employee.roles.findIndex(
      (item) => item.name == 'Manager',
    );

    if (!(indexEmployee != -1 && indexManager != -1)) {
      if (indexManager != -1) {
        salary.baseSalary = 20000;
      } else if (indexEmployee != -1) {
        salary.baseSalary = 18000;
      }
    } else {
      salary.baseSalary = 23000;
    }

    salary.increaseSalary = createSalaryDto.increaseSalary;
    salary.salaryReduc = createSalaryDto.salaryReduc;
    // const workH = await this.checkTimesRepository.findOneBy({
    //   id: createSalaryDto.employee_Id,
    // });
    // if (workH.workHours > 8) {
    //   salary.increaseSalary += (workH.workHours - 8) * 150;
    //   salary.salaryReduc = createSalaryDto.salaryReduc;
    // } else if (workH.workHours < 8 && workH.workHours > 0) {
    //   //fix condition for timework = 0
    //   salary.increaseSalary = createSalaryDto.increaseSalary;
    //   salary.salaryReduc += (8 - workH.workHours) * 50;
    // } else {
    //   salary.increaseSalary = createSalaryDto.increaseSalary;
    //   salary.salaryReduc = createSalaryDto.salaryReduc;
    // }
    salary.netSalary =
      salary.baseSalary + salary.increaseSalary - salary.salaryReduc;
    salary.status = 'Pending';
    console.log(salary.salaryReduc);
    return this.salarysRepository.save(salary);
  }

  findAll() {
    return this.salarysRepository.find({
      relations: {
        employee: true,
      },
      order: {
        id: 'ASC',
      },
    });
  }

  findOne(id: number) {
    return this.salarysRepository.findOne({
      where: { id },
      relations: {
        employee: true,
      },
    });
  }

  async update(id: number, updateSalaryDto: UpdateSalaryDto) {
    const salary = await this.salarysRepository.findOneOrFail({
      where: { id },
    });
    console.log(salary);
    const employee = await this.empsRepository.findOneBy({
      id: salary.employee_Id,
    });
    console.log(employee);
    console.log(updateSalaryDto);

    salary.payDate = new Date();
    if (updateSalaryDto.increaseSalary !== undefined) {
      if (employee.timeWork > 160) {
        salary.increaseSalary =
          (employee.timeWork - 160) * 150 + updateSalaryDto.increaseSalary;
      } else {
        salary.increaseSalary = updateSalaryDto.increaseSalary;
      }
    }
    if (updateSalaryDto.salaryReduc !== undefined) {
      if (employee.timeWork < 160 && employee.timeWork > 0) {
        //fix this condition can update
        salary.salaryReduc =
          (160 - employee.timeWork) * 50 + updateSalaryDto.salaryReduc;
      } else {
        salary.salaryReduc = updateSalaryDto.salaryReduc;
      }
    }
    // if (
    //   updateSalaryDto.increaseSalary !== undefined &&
    //   updateSalaryDto.salaryReduc !== undefined
    // ) {
    //   if (employee.timeWork > 160) {
    //     salary.increaseSalary =
    //       (employee.timeWork - 160) * 150 + updateSalaryDto.increaseSalary;
    //     salary.salaryReduc = updateSalaryDto.salaryReduc;
    //   } else if (employee.timeWork < 160) {
    //     salary.increaseSalary = updateSalaryDto.increaseSalary;
    //     salary.salaryReduc =
    //       (160 - employee.timeWork) * 50 + updateSalaryDto.salaryReduc;
    //   } else {
    //     salary.increaseSalary = updateSalaryDto.increaseSalary;
    //     salary.salaryReduc = updateSalaryDto.salaryReduc;
    //   }
    // }
    if (updateSalaryDto.status !== undefined) {
      salary.status = updateSalaryDto.status;
    }
    salary.netSalary =
      salary.baseSalary + salary.increaseSalary - salary.salaryReduc;
    await this.salarysRepository.update(id, salary);
    const result = await this.salarysRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const delSalary = await this.salarysRepository.findOneOrFail({
      where: { id },
    });
    await this.salarysRepository.remove(delSalary);
    return delSalary;
  }
}
