import { Module } from '@nestjs/common';
import { SalarysService } from './salarys.service';
import { SalarysController } from './salarys.controller';
import { Salary } from './entities/salary.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Role } from 'src/roles/entities/role.entity';
import { CheckTime } from 'src/checktime/entities/checktime.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Salary, User, Role, CheckTime])],
  controllers: [SalarysController],
  providers: [SalarysService],
  exports: [SalarysService],
})
export class SalarysModule {}
