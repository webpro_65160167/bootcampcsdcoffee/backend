import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { MemberService } from 'src/member/member.service';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private membersService: MemberService,
    private jwtService: JwtService,
    // eslint-disable-next-line prettier/prettier
  ) {}

  async signIn(email: string, pass: string): Promise<any> {
    try {
      // try {
      const user = await this.usersService.findOneByEmail(email);
      const isMatch = await bcrypt.compare(pass, user?.password);
      if (!isMatch) {
        throw new UnauthorizedException();
      }
      const payload = { id: user.id, email: user.email };
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...result } = user;
      return {
        user: result,
        access_token: await this.jwtService.signAsync(payload),
      };
      // } catch {
      //   const member = await this.membersService.findOneByTel(email);
      //   const isMatch = await bcrypt.compare(pass, member.password);
      //   console.log(pass);
      //   console.log(member.password);
      //   if (!isMatch) {
      //     throw new UnauthorizedException();
      //   }
      //   const payload = { id: member.id, email: member.tel };
      //   // eslint-disable-next-line @typescript-eslint/no-unused-vars
      //   const { password, ...result } = member;
      //   return {
      //     member: result,
      //     access_token: await this.jwtService.signAsync(payload),
      //   };
      // }
    } catch (e) {
      throw new UnauthorizedException();
    }
  }
}
