import { Order } from 'src/orders/entities/order.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  condition: string;

  @Column()
  percentDiscount: number;

  @Column()
  priceDiscount: number;

  @Column()
  minQty: number;

  @Column()
  minPrice: number;

  @Column()
  start_date: string;

  @Column()
  end_date: string;

  @Column({ default: 'noimage.png' })
  image: string;

  @Column()
  member: boolean;

  @OneToMany(() => Order, (order) => order.promotion, { onDelete: 'CASCADE' })
  order: Order;
}
