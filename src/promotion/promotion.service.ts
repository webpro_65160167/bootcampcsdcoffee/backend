import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Repository } from 'typeorm';
import { Promotion } from './entities/promotion.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class PromotionsService {
  constructor(
    @InjectRepository(Promotion)
    private promotionsRepository: Repository<Promotion>,
  ) {}
  create(createPromotionDto: CreatePromotionDto, file: string) {
    const promotion = new Promotion();
    console.log(createPromotionDto);
    promotion.name = createPromotionDto.name;
    promotion.condition = createPromotionDto.condition;
    promotion.priceDiscount = parseFloat(createPromotionDto.priceDiscount);
    promotion.percentDiscount = parseFloat(createPromotionDto.percentDiscount);
    promotion.minQty = parseFloat(createPromotionDto.minQty);
    promotion.minPrice = parseFloat(createPromotionDto.minPrice);
    promotion.start_date = createPromotionDto.start_date;
    promotion.end_date = createPromotionDto.end_date;
    if (createPromotionDto.member == 'true') {
      promotion.member = true;
    } else {
      promotion.member = false;
    }
    promotion.image = file;
    console.log(promotion);
    return this.promotionsRepository.save(promotion);
  }

  findAll() {
    return this.promotionsRepository.find();
  }

  findOne(id: number) {
    return this.promotionsRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    const promotion = new Promotion();
    promotion.name = updatePromotionDto.name;
    promotion.condition = updatePromotionDto.condition;
    promotion.priceDiscount = parseFloat(updatePromotionDto.priceDiscount);
    promotion.percentDiscount = parseFloat(updatePromotionDto.percentDiscount);
    promotion.minQty = parseFloat(updatePromotionDto.minQty);
    promotion.minPrice = parseFloat(updatePromotionDto.minPrice);
    promotion.start_date = updatePromotionDto.start_date;
    promotion.end_date = updatePromotionDto.end_date;
    if (updatePromotionDto.member == 'true') {
      promotion.member = true;
    } else {
      promotion.member = false;
    }
    console.log(promotion);
    if (updatePromotionDto.image && updatePromotionDto.image !== '') {
      promotion.image = updatePromotionDto.image;
    }
    const updatePromotion = await this.promotionsRepository.findOneOrFail({
      where: { id },
    });
    updatePromotion.name = promotion.name;
    updatePromotion.condition = promotion.condition;
    updatePromotion.priceDiscount = promotion.priceDiscount;
    updatePromotion.percentDiscount = promotion.percentDiscount;
    updatePromotion.minQty = promotion.minQty;
    updatePromotion.minPrice = promotion.minPrice;
    updatePromotion.start_date = promotion.start_date;
    updatePromotion.end_date = promotion.end_date;
    updatePromotion.image = promotion.image;
    updatePromotion.member = promotion.member;
    await this.promotionsRepository.save(updatePromotion);
    const result = await this.promotionsRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deletePromotion = await this.promotionsRepository.findOneByOrFail({
      id,
    });
    await this.promotionsRepository.remove(deletePromotion);
    return deletePromotion;
  }
}
