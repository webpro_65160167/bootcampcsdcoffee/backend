import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Promotion } from './entities/promotion.entity';
import { JwtModule } from '@nestjs/jwt';
import { JwtService } from '@nestjs/jwt';
import { PromotionsController } from 'src/promotion/promotion.controller';
import { PromotionsService } from 'src/promotion/promotion.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Promotion]),
    JwtModule.register({
      secret: 'your_secret_key',
      signOptions: { expiresIn: '1h' },
    }),
  ],
  controllers: [PromotionsController],
  providers: [PromotionsService, JwtService],
  exports: [PromotionsService],
})
export class PromotionModule {}
