export class CreatePromotionDto {
  name: string;
  condition: string;
  percentDiscount: string;
  priceDiscount: string;
  minQty: string;
  minPrice: string;
  start_date: string;
  end_date: string;
  member: string;
  image: string;
}
