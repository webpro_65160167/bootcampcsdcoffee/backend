import { PromotionsService } from 'src/promotion/promotion.service';
import { PromotionsController } from './promotion.controller';
import { Test, TestingModule } from '@nestjs/testing';

describe('PromotionController', () => {
  let controller: PromotionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PromotionsController],
      providers: [PromotionsService],
    }).compile();

    controller = module.get<PromotionsController>(PromotionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
