import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { OrderItem } from './orderItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Member } from 'src/member/entities/member.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  total: number;

  @Column()
  qty: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @Column({ nullable: true }) // Set nullable to true
  amount?: number;

  @Column()
  change: number;

  @Column()
  userId: number;

  @Column({ nullable: true }) // Set nullable to true
  memberId: number;

  @Column({ nullable: true }) // Set nullable to true
  promotionId: number;

  @Column()
  paymentType: string;

  @Column({ nullable: true })
  memberDiscount: number;

  @Column({ nullable: true })
  promotionDiscount: number;

  @Column({ nullable: true })
  totalNet: number;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  receiptItems: OrderItem[];

  @ManyToOne(() => User, (user) => user.orders)
  user: User;

  @ManyToOne(() => Member, (member) => member.order, {
    onDelete: 'CASCADE',
  })
  member?: Member;

  @ManyToOne(() => Promotion, (promotion) => promotion.order, {
    onDelete: 'CASCADE',
  })
  promotion?: Promotion[];
}
