export class CreateOrderDto {
  orderItems: {
    category: string;
    sweet: number;
    productId: number;
    qty: number;
  }[];
  promotionId: number;
  memberId: number;
  userId: number;
  memberDiscount: number;
  promotionDiscount: number;
  change: number;
  paymentType: string;
  amount: number;
  totalBefore: number;
  total: number;
}
