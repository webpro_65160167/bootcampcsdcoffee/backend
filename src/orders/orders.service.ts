import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { Repository } from 'typeorm';
import { OrderItem } from './entities/orderItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Product } from 'src/products/entities/product.entity';
// import { Member } from 'src/member/entities/member.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order) private ordersRepository: Repository<Order>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    // @InjectRepository(Member) private memberRepository: Repository<Member>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    try {
      const order = new Order();
      const user = await this.usersRepository.findOneBy({
        id: createOrderDto.userId,
      });

      // const member = await this.memberRepository.findOneBy({
      //   id: createOrderDto.memberId,
      // });

      // order.member = member;
      order.user = user;

      if (createOrderDto.promotionId) {
        order.promotionId = createOrderDto.promotionId;
      }
      if (createOrderDto.memberId) {
        order.memberId = createOrderDto.memberId;
      }
      order.userId = createOrderDto.userId;
      order.change = createOrderDto.change;
      order.paymentType = createOrderDto.paymentType;
      order.amount = createOrderDto.amount;
      order.totalNet = createOrderDto.total;
      order.change = createOrderDto.change;
      order.memberDiscount = createOrderDto.memberDiscount;
      order.promotionDiscount = createOrderDto.promotionDiscount;
      order.total = createOrderDto.totalBefore;
      order.qty = 0;
      order.receiptItems = [];

      for (const oi of createOrderDto.orderItems) {
        const orderItem = new OrderItem();
        orderItem.product = await this.productsRepository.findOneBy({
          id: oi.productId,
        });
        let Price = orderItem.product.price;
        if (oi.category === 'hot') {
          Price += 0;
        } else if (oi.category === 'cool') {
          Price += 5;
        } else {
          Price += 10;
        }
        orderItem.name = orderItem.product.name;
        orderItem.price = Price;
        orderItem.sweet = oi.sweet;
        orderItem.category = oi.category;
        orderItem.qty = oi.qty;
        await this.orderItemsRepository.save(orderItem);
        order.receiptItems.push(orderItem);
        order.qty += orderItem.qty;
      }

      return this.ordersRepository.save(order);
    } catch (error) {
      console.log(error);
    }
  }

  findAll() {
    return this.ordersRepository.find({
      relations: {
        receiptItems: true,
        user: true,
        member: true,
      },
      order: {
        id: 'DESC',
      },
    });
  }

  findOne(id: number) {
    return this.ordersRepository.findOneOrFail({
      where: { id },
      relations: { receiptItems: true },
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const deleteOrder = await this.ordersRepository.findOneOrFail({
      where: { id },
    });
    await this.ordersRepository.remove(deleteOrder);

    return deleteOrder;
  }
}
