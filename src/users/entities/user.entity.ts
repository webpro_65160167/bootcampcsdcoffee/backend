import { Branch } from 'src/branchs/entities/branch.entity';
import { CheckStock } from 'src/check-stock/entities/check-stock.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Role } from 'src/roles/entities/role.entity';
import { Salary } from 'src/salarys/entities/salary.entity';
import { StockReceipt } from 'src/stock-receipt/entities/StockReceipt.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column({
    name: 'full_name',
    default: '',
  })
  fullName: string;

  @Column({ select: false })
  password: string;

  @Column()
  gender: string;

  @Column({ default: 'noimage.png' })
  image: string;

  @Column({ default: '0999999999' })
  tel: string;

  @Column({ default: 0 })
  timeWork: number;

  @Column({ default: '0999999999' })
  bankAcc: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToMany(() => Role, (role) => role.users, { cascade: true })
  @JoinTable()
  roles: Role[];

  @OneToMany(() => Order, (order) => order.user)
  orders: Order[];

  @OneToMany(() => Salary, (salary) => salary.employee)
  salarys: Salary[];

  @OneToMany(() => StockReceipt, (stockReceipt) => stockReceipt.employee)
  stockReceipts: StockReceipt[];

  @OneToMany(() => CheckStock, (checkStock) => checkStock.employee)
  checkStocks: CheckStock[];

  @ManyToOne(() => Branch, (branch) => branch.user)
  branch: Branch[];
}
