import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from 'src/roles/entities/role.entity';
import * as bcrypt from 'bcrypt';
import { create } from 'domain';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}
  async create(createUserDto: CreateUserDto) {
    const user = new User();
    user.email = createUserDto.email;
    user.fullName = createUserDto.fullName;
    user.gender = createUserDto.gender;
    user.branch = JSON.parse(createUserDto.branch);
    const saltOrRounds = 10;
    user.password = await bcrypt.hash(createUserDto.password, saltOrRounds);
    user.tel = createUserDto.tel;
    user.bankAcc = createUserDto.bankAcc;
    user.timeWork = createUserDto.timeWork;
    user.roles = JSON.parse(createUserDto.roles);
    if (createUserDto.image && createUserDto.image !== '') {
      user.image = createUserDto.image;
    }
    user.branch = JSON.parse(createUserDto.branch);
    console.log(user);
    return this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.find({
      relations: { roles: true, branch: true },
    });
    return this.usersRepository.find({
      relations: { roles: true, branch: true },
    });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { roles: true, branch: true },
    });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOneOrFail({
      where: { email },
      relations: { roles: true, branch: true },
      select: {
        id: true,
        email: true,
        fullName: true,
        gender: true,
        password: true,
        roles: true,
        image: true,
      },
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = new User();
    user.email = updateUserDto.email;
    user.fullName = updateUserDto.fullName;
    user.gender = updateUserDto.gender;
    user.password = updateUserDto.password;
    user.tel = updateUserDto.tel;
    user.bankAcc = updateUserDto.bankAcc;
    user.timeWork = updateUserDto.timeWork;
    user.roles = JSON.parse(updateUserDto.roles);
    if (updateUserDto.image && updateUserDto.image !== '') {
      user.image = updateUserDto.image;
    }
    user.branch = JSON.parse(updateUserDto.branch);
    const updateUser = await this.usersRepository.findOneOrFail({
      where: { id },
      relations: { roles: true },
    });
    updateUser.email = user.email;
    updateUser.fullName = user.fullName;
    updateUser.gender = user.gender;
    updateUser.password = user.password;
    updateUser.image = user.image;
    updateUser.tel = user.tel;
    updateUser.bankAcc = user.bankAcc;
    updateUser.timeWork = user.timeWork;
    for (const r of user.roles) {
      const searchRole = updateUser.roles.find((role) => role.id === r.id);
      if (!searchRole) {
        const newRole = await this.rolesRepository.findOneBy({ id: r.id });
        updateUser.roles.push(newRole);
      }
    }
    for (let i = 0; i < updateUser.roles.length; i++) {
      const index = user.roles.findIndex(
        (role) => role.id === updateUser.roles[i].id,
      );
      if (index < 0) {
        updateUser.roles.splice(index, 1);
      }
    }
    updateUser.branch = user.branch;
    await this.usersRepository.save(updateUser);
    const result = await this.usersRepository.findOne({
      where: { id },
      relations: { roles: true, branch: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteUser = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    await this.usersRepository.remove(deleteUser);

    return deleteUser;
  }
}
