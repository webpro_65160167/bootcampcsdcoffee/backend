export class CreateUserDto {
  email: string;
  fullName: string;
  password: string;
  gender: string;
  roles: string;
  image: string;
  bankAcc: string;
  timeWork: number;
  tel: string;
  branch: string;
}
