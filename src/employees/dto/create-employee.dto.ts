export class CreateEmployeeDto {
  image: string;
  fullName: string;
  email: string;
  bankAcc: string;
  gender: string;
  tel: string;
  password: string;
  role: string;
}
