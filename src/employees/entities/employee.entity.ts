import { Role } from 'src/roles/entities/role.entity';
import { Salary } from 'src/salarys/entities/salary.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 'noimage.png' })
  image: string;

  @Column({
    name: 'full_name',
    default: '',
  })
  fullName: string;

  @Column()
  email: string;

  @Column()
  bankAcc: string;

  @Column()
  gender: string;

  @Column()
  tel: string;

  @Column()
  password: string;

  @Column()
  timeWork: number;

  @Column()
  role: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToMany(() => Role, (role) => role.employees, { cascade: true })
  @JoinTable()
  roles: Role[];

  @OneToMany(() => Salary, (salary) => salary.employee)
  salarys: Salary[];
}
