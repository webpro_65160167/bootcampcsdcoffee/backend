import { Injectable } from '@nestjs/common';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from 'src/roles/entities/role.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee) private usersRepository: Repository<Employee>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}
  async create(createEmployeeDto: CreateEmployeeDto) {
    const employee = new Employee();
    employee.email = createEmployeeDto.email;
    employee.fullName = createEmployeeDto.fullName;
    employee.gender = createEmployeeDto.gender;
    employee.bankAcc = createEmployeeDto.bankAcc;
    employee.tel = createEmployeeDto.tel;

    const saltOrRounds = 10;
    employee.password = await bcrypt.hash(
      createEmployeeDto.password,
      saltOrRounds,
    );

    employee.roles = JSON.parse(createEmployeeDto.role);
    if (createEmployeeDto.image && createEmployeeDto.image !== '') {
      employee.image = createEmployeeDto.image;
    }
    return this.usersRepository.save(employee);
  }

  findAll() {
    return this.usersRepository.find({ relations: { roles: true } });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOneOrFail({
      select: {
        id: true,
        email: true,
        fullName: true,
        gender: true,
        password: true,
        roles: true,
      },
      where: { email },
    });
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    const user = new Employee();
    user.email = updateEmployeeDto.email;
    user.fullName = updateEmployeeDto.fullName;
    user.gender = updateEmployeeDto.gender;
    user.password = updateEmployeeDto.password;
    user.roles = JSON.parse(updateEmployeeDto.role);
    if (updateEmployeeDto.image && updateEmployeeDto.image !== '') {
      user.image = updateEmployeeDto.image;
    }
    const updateUser = await this.usersRepository.findOneOrFail({
      where: { id },
      relations: { roles: true },
    });
    updateUser.email = user.email;
    updateUser.fullName = user.fullName;
    updateUser.gender = user.gender;
    updateUser.password = user.password;
    updateUser.image = user.image;
    for (const r of user.roles) {
      const searchRole = updateUser.roles.find((role) => role.id === r.id);

      if (!searchRole) {
        const newRole = await this.rolesRepository.findOneBy({ id: r.id });
        updateUser.roles.push(newRole);
      }
    }

    for (let i = 0; i < updateUser.roles.length; i++) {
      const index = user.roles.findIndex(
        (role) => role.id === updateUser.roles[i].id,
      );

      if (index < 0) {
        updateUser.roles.splice(i, 1);
      }
    }

    await this.usersRepository.save(updateUser);

    const result = await this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    await this.usersRepository.remove(deleteProduct);

    return deleteProduct;
  }
}
