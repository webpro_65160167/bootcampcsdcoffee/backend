import { Stock } from 'src/Stock/entities/Stock.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { StockReceipt } from './StockReceipt.entity';

@Entity()
export class StockReceiptItems {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  quantity: number;

  @Column()
  value: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Stock, (stock) => stock.stockReceiptItems)
  stock: Stock;

  @ManyToOne(
    () => StockReceipt,
    (stockReceipt) => stockReceipt.stockReceiptItems,
    {
      onDelete: 'CASCADE',
    },
  )
  stockReceipt: StockReceipt;
}
