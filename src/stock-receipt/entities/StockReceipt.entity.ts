import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { StockReceiptItems } from './StockReceiptItem.entity';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class StockReceipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  receiptDate: Date;

  @Column()
  totalPrice: number;

  @Column()
  totalQuantity: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => StockReceiptItems,
    (stockReceiptItem) => stockReceiptItem.stockReceipt,
  )
  stockReceiptItems: StockReceiptItems[];

  @ManyToOne(() => User, (employee) => employee.stockReceipts)
  employee: User;

  @Column()
  employeeName: string;

  @Column()
  employeeId: number;
}
