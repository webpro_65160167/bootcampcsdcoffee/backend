import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateStockReceiptDto } from './dto/update-StockReceipt.dto';
import { StockReceipt } from './entities/StockReceipt.entity';
import { CreateStockReceiptDto } from './dto/create-StockReceipt.dto';
import { StockReceiptItems } from './entities/StockReceiptItem.entity';
import { Stock } from 'src/Stock/entities/Stock.entity';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class StockReceiptService {
  constructor(
    @InjectRepository(StockReceipt)
    private stockReceiptRepository: Repository<StockReceipt>,
    @InjectRepository(StockReceiptItems)
    private stockReceiptItemRepository: Repository<StockReceiptItems>,
    @InjectRepository(Stock) private stockRepository: Repository<Stock>,
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}
  async create(createStockReceiptDto: CreateStockReceiptDto) {
    const stockReceipt = new StockReceipt();
    const user = await this.usersRepository.findOneBy({
      id: createStockReceiptDto.userId,
    });
    stockReceipt.employee = user;
    stockReceipt.employeeId = user.id;
    stockReceipt.employeeName = user.fullName;
    stockReceipt.totalPrice = 0;
    stockReceipt.totalQuantity = 0;
    stockReceipt.stockReceiptItems = [];
    stockReceipt.receiptDate = new Date();
    for (const si of createStockReceiptDto.stockReceiptItems) {
      const stockReceiptItem = new StockReceiptItems();
      stockReceiptItem.stock = await this.stockRepository.findOneBy({
        id: si.stockId,
      });
      stockReceiptItem.name = stockReceiptItem.stock.name;
      stockReceiptItem.price = si.price;
      stockReceiptItem.quantity = si.quantity;
      stockReceiptItem.value = si.price * si.quantity;
      await this.stockReceiptItemRepository.save(stockReceiptItem);
      stockReceipt.totalPrice += stockReceiptItem.value;
      stockReceipt.totalQuantity += stockReceiptItem.quantity;
      stockReceipt.stockReceiptItems.push(stockReceiptItem);
      const stock = await this.stockRepository.findOneBy({
        id: si.stockId,
      });
      stock.name = stock.name;
      stock.priceUnit = si.price;
      stock.quantity += si.quantity;
      stock.value = stock.priceUnit * stock.quantity;
      await this.stockRepository.save(stock);
    }
    return this.stockReceiptRepository.save(stockReceipt);
  }
  findAll() {
    return this.stockReceiptRepository.find({
      relations: ['stockReceiptItems', 'employee', 'employee.branch'],
      order: {
        id: 'desc',
      },
    });
  }
  findOne(id: number) {
    return this.stockReceiptRepository.find({
      where: { id },
      relations: { stockReceiptItems: true },
    });
  }

  async update(id: number, updateStockReceiptDto: UpdateStockReceiptDto) {
    return updateStockReceiptDto;
  }
  async remove(id: number) {
    const deleteStock = await this.stockReceiptRepository.findOneOrFail({
      where: { id },
    });
    await this.stockReceiptRepository.remove(deleteStock);
    return deleteStock;
  }

  async stockItemValueMoreThanAvg(month: string, year: string) {
    try {
      const result = await this.stockReceiptRepository.query(
        `CALL stockItemValueMoreThanAvg('${month}','${year}');`,
      );
      return result[0];
    } catch (error) {
      console.log(error);
    }
  }

  async getStockValueMoreThanAvg() {
    try {
      const result = await this.stockReceiptRepository.query(
        'SELECT * FROM getStockValueMoreThanAvg',
      );
      return result;
    } catch (error) {
      console.log(error);
    }
  }
}
