import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Patch,
} from '@nestjs/common';
import { UpdateStockReceiptDto } from './dto/update-StockReceipt.dto';
import { StockReceiptService } from './stock-receipt.service';
import { CreateStockReceiptDto } from './dto/create-StockReceipt.dto';

@Controller('stock-receipt')
export class StockReceiptController {
  constructor(private readonly stockReceiptService: StockReceiptService) {}
  @Get('ShowAvg')
  getStockValueMoreThanAvg() {
    return this.stockReceiptService.getStockValueMoreThanAvg();
  }

  @Get('CheckAvg/:month/:year')
  showStatusStock(@Param('month') month: string, @Param('year') year: string) {
    return this.stockReceiptService.stockItemValueMoreThanAvg(month, year);
  }

  @Post()
  create(@Body() createStockReceiptDto: CreateStockReceiptDto) {
    return this.stockReceiptService.create(createStockReceiptDto);
  }

  @Get()
  findAll() {
    return this.stockReceiptService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.stockReceiptService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateStockReceiptDto: UpdateStockReceiptDto,
  ) {
    return this.stockReceiptService.update(+id, updateStockReceiptDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.stockReceiptService.remove(+id);
  }
}
