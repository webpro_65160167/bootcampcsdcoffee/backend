export class CreateStockReceiptDto {
  stockReceiptItems: {
    stockId: number;
    quantity: number;
    price: number;
  }[];
  userId: number;
}
