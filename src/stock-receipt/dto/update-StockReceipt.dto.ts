import { PartialType } from '@nestjs/swagger';
import { CreateStockReceiptDto } from './create-StockReceipt.dto';

export class UpdateStockReceiptDto extends PartialType(CreateStockReceiptDto) {}
