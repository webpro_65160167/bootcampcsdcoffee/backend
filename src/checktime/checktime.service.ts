import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, FindOneOptions, Repository } from 'typeorm';
import { CheckTime } from './entities/checktime.entity';
import { CreateCheckTimeDto } from './dto/create-checktime.dto';

@Injectable()
export class CheckTimeService {
  constructor(
    @InjectRepository(CheckTime)
    private readonly checkTimeRepository: Repository<CheckTime>,
  ) {}

  async findAll(): Promise<CheckTime[]> {
    return this.checkTimeRepository.find();
  }

  async checkIn(checkInData: CreateCheckTimeDto): Promise<CheckTime> {
    // Convert CreateCheckTimeDto to DeepPartial<CheckTime>
    const checkTimeData: DeepPartial<CheckTime> = {
      ...checkInData,
      status: checkInData.status ? 'intime' : 'late', // Convert boolean to string
    };
    // Save data to database
    return this.checkTimeRepository.save(checkTimeData);
  }

  async checkOut(
    id: number,
    checkOutData: Partial<CheckTime>,
  ): Promise<CheckTime> {
    const options: FindOneOptions<CheckTime> = { where: { id } };
    const checkTime = await this.checkTimeRepository.findOne(options);
    if (!checkTime) {
      throw new Error('Check time not found');
    }
    Object.assign(checkTime, checkOutData);
    return await this.checkTimeRepository.save(checkTime);
  }
}
