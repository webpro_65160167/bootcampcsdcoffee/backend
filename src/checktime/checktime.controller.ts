import { Controller, Get, Post, Body, Put, Param } from '@nestjs/common';
import { CheckTime } from './entities/checktime.entity';
import { CreateCheckTimeDto } from './dto/create-checktime.dto';
import { CheckTimeService } from './checktime.service';

@Controller('checktime')
export class CheckTimeController {
  constructor(private readonly checkTimeService: CheckTimeService) {}

  @Get()
  async findAll(): Promise<CheckTime[]> {
    return this.checkTimeService.findAll();
  }

  @Post('checkin')
  async checkIn(@Body() checkInData: CreateCheckTimeDto): Promise<CheckTime> {
    return this.checkTimeService.checkIn(checkInData);
  }

  @Put('checkout/:id')
  async checkOut(
    @Param('id') id: number,
    @Body() checkOutData: Partial<CheckTime>,
  ) {
    return await this.checkTimeService.checkOut(id, checkOutData);
  }
}
