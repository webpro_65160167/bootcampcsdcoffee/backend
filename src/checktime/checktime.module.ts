import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckTime } from './entities/checktime.entity';
import { CheckTimeController } from './checktime.controller';
import { CheckTimeService } from './checktime.service';

@Module({
  imports: [TypeOrmModule.forFeature([CheckTime])],
  controllers: [CheckTimeController],
  providers: [CheckTimeService],
})
export class CheckTimeModule {}
