import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class CheckTime {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  employeeId: number;

  @Column()
  name: string;

  @Column()
  role: string;

  @Column({ type: 'datetime' })
  checkIn: Date;

  @Column({ type: 'datetime', nullable: true })
  checkOut: Date;

  @Column({ type: 'float', nullable: true })
  workHours: number;

  // สถานะการเช็คเวลา เช่น intime, late, etc.
  @Column()
  status: string;
}
