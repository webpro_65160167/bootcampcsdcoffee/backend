export class CreateCheckTimeDto {
  id: number;
  name: string;
  password: string;
  role: string;
  checkIn: Date;
  checkOut: Date;
  workHours: number;
  status: boolean;
}
