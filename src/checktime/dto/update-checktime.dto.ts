import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckTimeDto } from './create-checktime.dto';

export class UpdateCheckTimeDto extends PartialType(CreateCheckTimeDto) {}
