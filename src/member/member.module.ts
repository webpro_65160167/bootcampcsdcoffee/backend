import { Module } from '@nestjs/common';
import { MemberController } from './member.controller';
import { MemberService } from './member.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { Member } from './entities/member.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Member]),
    JwtModule.register({
      secret: 'your_secret_key', // Make sure to use your actual secret key here
      signOptions: { expiresIn: '1h' },
    }),
  ],
  controllers: [MemberController],
  providers: [MemberService], // Removed JwtService from here
  exports: [MemberService], // Export MemberService if it needs to be used outside this module
})
export class MemberModule {}
