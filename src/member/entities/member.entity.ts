import { Order } from 'src/orders/entities/order.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

@Entity()
export class Member {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'name',
    default: '',
  })
  name: string;

  @Column()
  tel: string;

  @Column()
  point: number;

  @Column({ select: false })
  password: string;

  @OneToMany(() => Order, (order) => order.member)
  order: Order;
}
