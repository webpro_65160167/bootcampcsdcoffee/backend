import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateMemberDto } from 'src/member/dto/create-member.dto';
import { UpdateMemberDto } from 'src/member/dto/update-member.dto';
import { Member } from 'src/member/entities/member.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class MemberService {
  constructor(
    @InjectRepository(Member)
    private memberRepository: Repository<Member>,
  ) {}
  async create(createMemberDto: CreateMemberDto) {
    const member = new Member();
    member.name = createMemberDto.name;
    member.point = createMemberDto.point;
    member.tel = createMemberDto.tel;
    const saltOrRounds = 10;
    member.password = await bcrypt.hash(createMemberDto.password, saltOrRounds);
    console.log(member.password);
    return this.memberRepository.save(member);
  }
  // this.ordersRepository.save(order);
  findAll() {
    return this.memberRepository.find();
  }

  findOne(id: number) {
    return this.memberRepository.findOneBy({ id });
  }

  async findOneByTel(tel: string) {
    return this.memberRepository.findOneOrFail({
      where: { tel },
      select: {
        id: true,
        name: true,
        tel: true,
        point: true,
        password: true,
      }, // Adjusted to an array of strings
    });
  }
  async findByTel(tel: string) {
    return await this.memberRepository.findOneBy({ tel });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    await this.memberRepository.findOneByOrFail({ id });
    await this.memberRepository.update(id, updateMemberDto);
    const updatedMember = await this.memberRepository.findOneBy({ id });
    return updatedMember;
  }

  async remove(id: number) {
    const removedMember = await this.memberRepository.findOneByOrFail({
      id,
    });
    await this.memberRepository.remove(removedMember);
    return removedMember;
  }
}
