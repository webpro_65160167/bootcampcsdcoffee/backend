import { Branch } from 'src/branchs/entities/branch.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Expense {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  billType: string;

  @Column()
  totalAmount: number;

  @CreateDateColumn()
  createDate: Date;

  @CreateDateColumn()
  created: Date;

  @Column()
  note: string;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Branch, (branch) => branch.expenses)
  branchName: Branch[];
}
