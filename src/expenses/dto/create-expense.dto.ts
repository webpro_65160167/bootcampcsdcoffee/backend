export class CreateExpenseDto {
  name: string;
  branchName: string;
  billType: string;
  totalAmount: string;
  createDate: Date;
  note: string;
}
