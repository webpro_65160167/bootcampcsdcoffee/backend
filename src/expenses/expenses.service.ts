import { Injectable } from '@nestjs/common';
import { CreateExpenseDto } from './dto/create-expense.dto';
import { UpdateExpenseDto } from './dto/update-expense.dto';
import { Expense } from './entities/expense.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ExpensesService {
  constructor(
    @InjectRepository(Expense)
    private expenseRepository: Repository<Expense>,
  ) {}

  async create(createExpenseDto: CreateExpenseDto) {
    const expense = new Expense();
    expense.name = createExpenseDto.name;
    expense.billType = createExpenseDto.billType;
    expense.totalAmount = parseFloat(createExpenseDto.totalAmount);
    expense.createDate = createExpenseDto.createDate;
    expense.note = createExpenseDto.note;
    expense.branchName = JSON.parse(
      JSON.stringify(createExpenseDto.branchName),
    );
    await this.expenseRepository.save(expense);
    return this.expenseRepository.find({ relations: ['branchName'] });
  }

  findAll() {
    return this.expenseRepository.find({
      relations: ['branchName'],
      order: { id: 'ASC' },
    });
  }

  findOne(id: number) {
    return this.expenseRepository.findOne({ where: { id } });
  }

  findAllByBranch(branchsId: number) {
    return this.expenseRepository.find({
      where: { branchName: { id: branchsId } },
      relations: { branchName: true },
      order: { name: 'ASC' },
    });
  }

  async update(id: number, updateExpenseDto: UpdateExpenseDto) {
    const updateExpense = await this.expenseRepository.findOneOrFail({
      where: { id },
    });
    updateExpense.name = updateExpenseDto.name;
    updateExpense.billType = updateExpenseDto.billType;
    updateExpense.totalAmount = parseFloat(updateExpenseDto.totalAmount);
    updateExpense.createDate = updateExpenseDto.createDate;
    updateExpense.note = updateExpenseDto.note;
    await this.expenseRepository.save(updateExpense);

    const result = await this.expenseRepository.findOne({
      where: { id },
      relations: { branchName: true },
    });

    return result;
  }

  async remove(id: number) {
    const deleteExpense = await this.expenseRepository.findOneOrFail({
      where: { id },
    });
    await this.expenseRepository.remove(deleteExpense);
    return deleteExpense;
  }

  async showMonthExp(month: string, year: string) {
    try {
      const result = await this.expenseRepository.query(
        `CALL showMonthExp('${month}','${year}');`,
      );

      return result[0];
    } catch (error) {
      console.log(error);
    }
  }

  async showYearExp(year: string) {
    try {
      const result = await this.expenseRepository.query(
        `CALL showYearExp('${year}');`,
      );
      console.log(year);
      return result[0];
    } catch (error) {
      console.log(error);
    }
  }
}
